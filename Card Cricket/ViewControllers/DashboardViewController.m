//
//  DashboardViewController.m
//  Card Cricket
//
//  Created by Gautham on 26/11/13.
//  Copyright (c) 2013 q98. All rights reserved.
//

#import "DashboardViewController.h"
#import "GameModesViewController.h"

@implementation DashboardViewController
@synthesize playButton;
@synthesize settingsButton, gameCenterButton;
@synthesize randomPlayers;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [mSingleton loadPlayerDetails];
    [self setupButtonStyle];
    [self setupICarousel];
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

# pragma mark -
# pragma mark LocalMethods
-(void)setupICarousel
{
    if(!carouselData)
        carouselData = [[NSMutableArray alloc] init];
    [carouselData removeAllObjects];
    
    carouselData = [mSingleton.allPlayerDetails mutableCopy];
   
    NSMutableArray *result = [carouselData mutableCopy];
    NSUInteger count = [result count];
    for (NSInteger i = ((NSInteger) count) - 1; i > 0; i--) {
        NSUInteger firstIndex = (NSUInteger)i;
        NSUInteger secondIndex = arc4random() % (NSUInteger)(i + 1);
        
        [result exchangeObjectAtIndex:firstIndex withObjectAtIndex:secondIndex];
    }
    
    int length = 15; // int length = [yourArray count];
    NSMutableArray *indexes = [[NSMutableArray alloc] initWithArray:result];
    NSMutableArray *shuffle = [[NSMutableArray alloc] initWithCapacity:length];
    while (length)
    {
        int index = rand()%length;
        [shuffle addObject:[indexes objectAtIndex:index]];
        [indexes removeObjectAtIndex:index];
        length --;
    }
    [carouselData removeAllObjects];
    [carouselData addObjectsFromArray:shuffle];
    
    randomPlayers.type = iCarouselTypeRotary;
    randomPlayers.bounces = FALSE;
    [randomPlayers reloadData];
}

-(void)setupButtonStyle
{
    expanded = FALSE;
    
    playButton.layer.borderWidth = 1.5;
    playButton.layer.borderColor = [UIColor whiteColor].CGColor;
    [self.view bringSubviewToFront:playButton];
}

-(void)playSound:(id)sender
{
    NSString *soundPath = [[NSBundle mainBundle] pathForResource:@"kick3" ofType:@"mp3"];
    NSURL *soundUrl = [NSURL fileURLWithPath:soundPath];
    SystemSoundID soundID;
    
    AudioServicesCreateSystemSoundID ((CFURLRef)CFBridgingRetain(soundUrl), &soundID);
    AudioServicesPlaySystemSound(soundID);
}

# pragma mark -
# pragma mark IBAction
-(IBAction)onPlay:(id)sender
{
   // [self performSelector:@selector(playSound:) withObject:sender];
    
    GameModesViewController *mGameModesViewController = [[GameModesViewController alloc] initWithNibName:@"GameModesViewController" bundle:nil];
    
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = @"rippleEffect";
    transition.subtype = kCATransitionFromTop;
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:mGameModesViewController animated:NO];

}

-(IBAction)onArcade:(id)sender
{
    [self performSelector:@selector(playSound:) withObject:sender];
}

-(IBAction)onMultiplayer:(id)sender
{
    [self performSelector:@selector(playSound:) withObject:sender];
    
    if (!multiPlayerAlert)
        multiPlayerAlert = [[UIAlertView alloc] initWithTitle:@"Multiplayer!" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Host Game",@"Join Game", nil];
    
    [multiPlayerAlert show];
}

-(IBAction)onSettings:(id)sender
{
    [self performSelector:@selector(playSound:) withObject:sender];
}

-(IBAction)onGameCenter:(id)sender
{
    [self performSelector:@selector(playSound:) withObject:sender];
}

# pragma mark -
# pragma mark UIAlerViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle: nil];
    if(buttonIndex == 1)
    {
        
    }
    else if(buttonIndex == 2)
    {
        
    }
}

#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return [carouselData count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    UIImageView *imageView = nil;
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 150, 180)];
        view.layer.cornerRadius = 5;
        view.layer.borderWidth = 2;
        view.layer.borderColor = [[UIColor whiteColor] CGColor];
        
        view.contentMode = UIViewContentModeCenter;
        imageView = [[UIImageView alloc] initWithFrame:view.bounds];
        imageView.backgroundColor = [UIColor clearColor];
        view.tag = index;
        [view addSubview:imageView];
    }
    else
    {
        //get a reference to the label in the recycled view
        if (([[view subviews] count] > 0) && ([[[view subviews] objectAtIndex:0] isKindOfClass:[UIImageView class]]))
        {
            imageView = [[view subviews] objectAtIndex:0];
        }
        else
        {
            imageView = [[UIImageView alloc] initWithFrame:view.bounds];
            imageView.backgroundColor = [UIColor clearColor];
            [view addSubview:imageView];
        }
    }
    
    NSDictionary *playerDict = [carouselData objectAtIndex:index];
    NSString *playerImage =[NSString stringWithFormat:@"%@.jpg",[playerDict valueForKey:@"Player ID"]];
    [imageView setImage:[UIImage imageNamed:playerImage]];
    
    return view;
}

@end
