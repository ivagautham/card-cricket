//
//  GameModesViewController.h
//  Card Cricket
//
//  Created by Gautham on 18/12/13.
//  Copyright (c) 2013 q98. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameModesViewController : UIViewController
{
    IBOutlet UIButton *btn_easy;
    IBOutlet UIButton *btn_medium;
    IBOutlet UIButton *btn_hard;
    
    IBOutlet UIButton *btn_gametype6;
    IBOutlet UIButton *btn_gametype10;
    IBOutlet UIButton *btn_gametype20;
}

-(IBAction)onBack:(id)sender;

@end
