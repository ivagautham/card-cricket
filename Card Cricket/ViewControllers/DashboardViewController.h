//
//  DashboardViewController.h
//  Card Cricket
//

//  Copyright (c) 2013 q98. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"

@interface DashboardViewController : UIViewController <UIAlertViewDelegate,iCarouselDataSource,iCarouselDelegate>
{
    BOOL expanded;
    UIAlertView *multiPlayerAlert;
    NSMutableArray *carouselData;
}

@property(nonatomic, strong) IBOutlet UIButton *playButton;

@property(nonatomic, strong) IBOutlet UIButton *settingsButton;
@property(nonatomic, strong) IBOutlet UIButton *gameCenterButton;

@property(nonatomic, strong) IBOutlet iCarousel *randomPlayers;

-(IBAction)onPlay:(id)sender;
-(IBAction)onArcade:(id)sender;

-(IBAction)onSettings:(id)sender;
-(IBAction)onGameCenter:(id)sender;

@end
