//
//  main.m
//  Card Cricket
//
//  Created by Gautham on 26/11/13.
//  Copyright (c) 2013 q98. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
