//
//  Singleton.h
//  Card Cricket
//
//  Created by VA Gautham  on 15-12-13.
//  Copyright (c) 2013 q98. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Singleton : NSObject
{
    NSMutableArray *allPlayerDetails;
}

@property(nonatomic, strong) NSMutableArray *allPlayerDetails;

+(Singleton *) getSingletonInstance;

-(void)loadPlayerDetails;

@end
