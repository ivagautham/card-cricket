//
//  Singleton.m
//  Card Cricket
//
//  Created by VA Gautham  on 15-12-13.
//  Copyright (c) 2013 q98. All rights reserved.
//

#import "Singleton.h"

static Singleton* instance;

@implementation Singleton
@synthesize allPlayerDetails;

+ (Singleton *) getSingletonInstance
{
	@synchronized([Singleton class])
    {
		if ( instance == nil )
        {
			instance = [[Singleton alloc] init];
		}
	}
	return instance;
}

-(void)loadPlayerDetails
{
    NSString* path = [[NSBundle mainBundle] pathForResource:@"allPlayerDetails" ofType:@"json"];
    NSString* content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:NULL];
    NSData* data = [content dataUsingEncoding:NSUTF8StringEncoding];
    
    if(!allPlayerDetails)
        allPlayerDetails = [[NSMutableArray alloc] init];
    [allPlayerDetails removeAllObjects];
    
    NSError *jsonError;
    allPlayerDetails = [[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError] mutableCopy];
}

@end
